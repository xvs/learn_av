<!--
 * @Copyright: 2024 Qujunyang
 * @Author: QuJunyang
 * @Date: 2024-01-25 18:05:12
 * @LastEditTime: 2024-02-01 14:02:24
 * @LastEditors: QuJunyang
 * @FilePath: \learn_av\README.md
 * @Description: 
-->
# learn_av

# run test
```
// 运行所有 workspace 的所有测试
cargo test
```

# run someone workspace
```
cargo run --bin tutorial1-pcm
```